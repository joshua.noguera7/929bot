#!/user/bin/python3.7

import discord
import secrets # File that stores login information for the bot
import shelve
import datetime
import ayncio


with db as shelve.open("929data"):
    if "929rs" in db:
        nine29ners = db["929rs"]
    else:
        print("929rs data not found using empty dictionary.")
        nine29ners = {}


def save(key, data):
    with db as shelve.open("929data"):
        db[key]=data


currentlist = []
pastlist = []


isit929 = False
async def isit929():
    while True:
        now = datetime.datetime.now()
        if (now.hour in (9, 21)) and (now.minute == 29):
            isit929=True
            while (now.hour in (9, 21)) and (now.minute == 29):
                await asyncio.sleep(0.1)
            isit929=False
            await client.recap()
            await asyncio.sleep(11*60*60 + 58*60 + 30) # Sleeps for 11 hours 58 minutes and 30 seconds.
        else:
            await asyncio.sleep(0.1)


class nine29er(id_code):
    self.currentstreak = 0
    self.points = 0
    self.maxstreak = 0
    self.count = 0
    self.id_code = id_code

    def trigger(self, first=False):
        if first:
            self.points = self.points + 1.5(1 + int(currentstreak/5))
        else:
            self.points = self.points + (1 + int(currentstreak/5))

class bot929(discord.Client, pastlist, currentlist):
    async def on_ready(self):
        print('Logged on as', self.user)

    async def process929(929author):
        if 929author.id not in currentlist:
            currentlist.append(929author.id)
            nine29ners[929author.id].trigger(len(currentlist)==1)
            save("929rs", nine29ners)
            
    async def recap(self):
        pass

    async def on_message(self, message):
        if message.author == self.user:
            return
        if message.content == '!ping':
            await message.channel.send('pong!')
        if (datetime.datetime.now().hour in (9, 21)) and datetime.datetime.now().minute == 29):
            if (message.content == "929") and (message.guild.id == 377637608848883723):
                process929(author)

client = bot929()
client.loop.create_task(isit929())
client.run(secrets.client_token)
